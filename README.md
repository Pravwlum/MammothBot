This is a branch off from [Nadeko 1.9 by Kwoth](https://gitlab.com/Kwoth/nadekobot/-/commits/1.9/) for the Ulala discord server. All credits to Kwoth#2452 for the original work, we have included the original license as well. 

We can can customize our self-hosted version to fit our needs, and collaborate on development here. Please reach out to Prav#3294 on Discord for questions.